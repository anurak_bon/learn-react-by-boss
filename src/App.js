import React, { Component } from 'react';
// import logo from './logo.svg';
import './App.css';
import { BrowserRouter } from 'react-router-dom'

// import LoginBox from './LoginBox';
import Todo from './Todos';
import Router from './Router'

class App extends Component {

  state = {
    products: []
  }

  addProduct = (newProduct) => this.setState({ 
    products: [ ...this.state.products, newProduct ] 
  })
  // addProduct = (newProduct) => console.log(newProduct)
 

  render() {
      return (
          <BrowserRouter>
              <Router products={{
                  products: this.state.products,
                  addProduct: this.addProduct
              }} />
          </BrowserRouter>
      )
  }
  // render() {
  //   return (
  //     <div className="App">
  //        {/* <header className="App-header">
  //          <img src={logo} className="App-logo" alt="logo" />
  //          <h1 className="App-title">Welcome to React</h1>
  //        </header>
  //        <p className="App-intro">
  //          To get started, edit <code>src/App.js</code> and save to reload.
  //       </p>
  //       <LoginBox title="My first login Box"/>
  //       <LoginBox title="My second login Box"/> */}

  //       <Todo />
  //     </div>
  //   );
  // }
}

export default App;
