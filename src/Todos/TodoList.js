// presentational component
import React from 'react';

// Stateless Component
const TodoList = (props) => (
    <ul>
        {
            props.todos.map((todo, index) => {
                return (<li key={index} >{todo}</li>)
            })
        }
    </ul>
)

export default TodoList