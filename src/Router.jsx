import React, { Component, Fragment} from "react";
import { Route, Switch } from 'react-router-dom';

import DataWrapper from './DataWrapper'

import Home from './Home';
import About from './About';
import FAQ from './FAQ';
import Navbar from './Header/Navbar';

import Product from './Products';

export default class Router extends Component {
    state = {  }
    render() {
        return (
            <Fragment>
                <Navbar/>
                <Route path='/' exact component={Home}/>
                {/* <Route path='/home' component={Home}/> */}
                <Route path='/about' component={About}/>
                <Route path='/faq' component={FAQ}/>

                <Route path='/products' component={DataWrapper(this.props.products, Home)}/>
                <Route path='/products/new' component={DataWrapper(this.props.products, Product)}/>
            </Fragment>
        );
    }
}