// presentational component
import React from 'react';

// Stateless Component
const ProductList = (props) => (
    <ul>
        {
            props.products.map((product, index) => {
                return (<li key={index} >{product.name} {product.price}</li>)
            })
        }
    </ul>
)

export default ProductList