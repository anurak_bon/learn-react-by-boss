import React, { Component } from 'react';

export default class  LoginBox extends Component {
    state = {  
        buttonName: 'Click me!',
        input: 'mo',
        nums: [1, 2, 3, 4, 5, 6],
        isLoading: true
    }

    // constructor(){
    //     super()
    //     this.buttonName = 'Click me'
    // }

    // ES7 can declare variable out of constructor
    // buttonName = 'Click me';
    
    // es6 bind
    clickHanle() {
        // alert('Click Handler')
        this.buttonName = 'bon button';
        this.forceUpdate();
    }

    // es7 built-in bind
    clickHandle2 = () => {
        this.buttonName = 'bon button';
        this.forceUpdate();
    }

    // 
    clickHandle3 = () => {
        const obj = {
            buttonName: 'bon button',
            input: 'nan'
        }
        this.setState(obj)
    }

    inputHandle = (event) => {
        const value = event.target.value;
        this.setState({input: value})
    }

    componentDidMount = () => {
        setTimeout( ()=>{
            this.setState({ isLoading: false})
        }, 3000)
    }

    render() {
        if(this.state.isLoading) {
            return (<div>Loading...</div>)
        }
        return (
            <div>
                <div>Login Box</div>
                <div>
                    {this.state.input}
                    {/* { this.props.title } */}
                    {/* <button onClick={this.clickHanle.bind(this)} >{this.buttonName}</button> */}
                    {/* <button onClick={this.clickHandle2} >{this.buttonName}</button> */}
                    <button onClick={this.clickHandle3} >{this.state.buttonName}</button>
                    <input type="text" value={this.state.input} onChange={this.inputHandle} />
                    <ul>
                        {
                            this.state.nums.map((num, index) => {
                                return (<li key={index} >{num}</li>)
                            })
                        }
                    </ul>
                </div>
            </div>
        );
    }
}