// Container Component
import React, { Component } from 'react';
import TodoList from './TodoList';
import TodoInput from './TodoInput';

export default class Todo  extends Component {
    state = {  
        inputText: '',
        todos: []
    }

    addTodo = (newTodo) => {
        this.setState({
            todos: [...this.state.todos, newTodo]
        })
    }

    render() {
        return (
            <div>
                <TodoInput onAddTodo={this.addTodo} />
                <TodoList todos={this.state.todos}/>
            </div>
        );
    }
}