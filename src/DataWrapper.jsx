import React, { Component } from 'react'

export default (data, InnerComponent) => class DataWrapper extends Component {
    render() {
        return (
            // <Todo {...this.props} data={this.props.todoState} />
            <InnerComponent {...this.props} data={data} />
        )
    }
}