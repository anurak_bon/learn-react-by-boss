// Container Component
import React, { Component } from 'react';
import ProductList from './ProductList';
import ProductInput from './ProductInput';

import Style from './style.css';

export default class Product  extends Component {
    state = {  
        inputText: '',
        products: []
    }

    addProduct = (newProduct) => {

        console.log(this.props);
        this.props.data.addProduct(newProduct);
        this.setState({
            products: [...this.state.products, newProduct]
        })
    }

    render() {
        return (
            <div className={Style.card}>
                <ProductInput onAddProduct={this.addProduct} />
                <ProductList products={this.state.products}/>
            </div>
        );
    }
}