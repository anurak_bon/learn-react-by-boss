// import React from 'react';
// import ReactDOM from 'react-dom';
// import './index.css';
// // import App from './App';
// import registerServiceWorker from './registerServiceWorker';

// import Router from './Router'
// import { BrowserRouter } from 'react-router-dom'

// // ReactDOM.render(<App />, document.getElementById('root'));
// ReactDOM.render(
//     <BrowserRouter>
//         <Router /> 
//     </BrowserRouter>,
//     document.getElementById('root'));
// registerServiceWorker();

import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'

ReactDOM.render(
    <App />,
    document.getElementById('root')
)