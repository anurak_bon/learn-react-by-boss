import React, { Component } from 'react';
import { Link } from 'react-router-dom';


export default class Navbar extends Component {
    state = {  }
    render() {
        return (
            <nav>
                <Link to='/home'>Home</Link>                
                <Link to='/about'>About</Link>
                <Link to='/faq'>FAQ</Link>
                <Link to='/products'>product List</Link>
                <Link to='/products/new'>Create Products</Link>
            </nav>            
        );
    }
}