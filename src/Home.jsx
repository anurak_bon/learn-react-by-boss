import React, { Component } from 'react';

import ProductList from './Products/ProductList';

export default class Home extends Component {
    state = {  }
    render() {
        console.log(this.props)
        return (
            <div>
                HOME
                <ProductList products={this.props.data.products}/>
            </div>
        );
    }
}