import React, { Component } from 'react';
import Style from './Todo.scss';
console.log(Style)
export default class TodoInput  extends Component {
    state = {  
        inputText: '',
    }

    inputTextBoxHandle = (event) => {
        const value = event.target.value;
        this.setState({inputText: value})
    }

    addHandle = (event) => {
        // prevent refresh page when submit
        event.preventDefault();
        this.props.onAddTodo(this.state.inputText);
        this.setState({
            inputText: ''
        })        
    }

    render () {
        return (
            <div className={Style.input}>
                <form onSubmit={this.addHandle}>
                    <input  type="text"
                        value={this.state.inputText} 
                        onChange={this.inputTextBoxHandle} />
                    <button type="submit">ADD</button>
                </form>            
            </div> 
        )
    }
}




