import React, { Component } from 'react';

export default class ProductInput  extends Component {
    state = {  
        inputTextProductName: '',
        inputTextProductPrice: ''
    }

    inputTextBoxProductNameHandle = (event) => {
        const value = event.target.value;
        this.setState({inputTextProductName: value})
    }

    inputTextBoxProductPriceHandle = (event) => {
        const value = event.target.value;
        this.setState({inputTextProductPrice: value})
    }

    addHandle = (event) => {
        // prevent refresh page when submit
        event.preventDefault();
        const product = {
            name: this.state.inputTextProductName,
            price: this.state.inputTextProductPrice
        }
        this.props.onAddProduct(product);
        
        this.setState({
            inputTextProductName: '',
            inputTextProductPrice: ''
        });
    }

    render () {
        return (
            <div>
                <form onSubmit={this.addHandle}>
                    <input  type="text"
                        value={this.state.inputTextProductName} 
                        onChange={this.inputTextBoxProductNameHandle} />

                    <input  type="text"
                    value={this.state.inputTextProductPrice} 
                    onChange={this.inputTextBoxProductPriceHandle} />

                    <button type="submit">ADD NEW PRODUCT</button>
                </form>            
            </div> 
        )
    }
}
